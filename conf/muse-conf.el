;;
;; Filename: muse-conf.el
;; Description: Setting for muse.el
;; Author: Hong Jin
;; Created: 2010-12-09 10:00
;; Last Updated: 2012-04-16 21:23:01
;;
;;
(message "%d: >>>>> Loading [ muse ] Customizations ...." step_no)
(setq step_no (1+ step_no))

(require 'muse)
(require 'muse-mode)
(require 'muse-html)     ; load publishing styles I use
(require 'muse-colors)
(require 'muse-wiki)
(require 'muse-latex)
(require 'muse-texinfo)
(require 'muse-docbook)
(require 'muse-project)


